package com.cnglobal.expeedio.common;

import java.util.Iterator;
import java.util.Set;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import jdk.internal.net.http.common.Log;

public class TopBar {
	WebDriver driver;
	
	public TopBar(WebDriver driver) {
		this.driver = driver;
	}
	
	@FindBy(xpath ="//img[@id='AppLogo']" )
	public WebElement img_appLogo;
	@FindBy(xpath ="//span/img[@class='header-profile-picture m--img-rounded m--marginless m--img-centered']")
	public WebElement img_verify_dashboadpage;
	//img[@src='/Profile/GetProfilePicture?t=637251080093842065']
	@FindBy(xpath ="//span[@id='user_email_info']")	
	public WebElement img_profile_picture;
	@FindBy(xpath ="//span[contains(text(),'Change profile picture')]")	
	public WebElement lnk_change_profile_picture;
	@FindBy(name = "ProfilePicture")
	public WebElement btn_choose_file_IN_profile_picture_uploadCopy;
	
	//@FindBy(xpath ="/html[1]/body[1]/div[5]/div[1]/div[1]/div[1]/div[2]/form[1]/div[1]/input[1]")
	@FindBy(xpath ="//input[@type='file']")
	//@FindBy(xpath ="//span[contains(text(),'You can select a JPG/JPEG/PNG file with a maximum 5MB size')]")
	public WebElement btn_choose_file_IN_profile_picture_upload;
	
	@FindBy(xpath ="//span[contains(text(),'Save')]")
	public WebElement btn_save_IN_profile_picture_upload;
	@FindBy(xpath ="//input[@type='file']/../../../../div[3]/button[contains(text(),'Cancel')]")
	public WebElement btn_cancel_IN_profile_picture_upload;
	public void getQuote() {
		//Log.info("");
	}
	public void tracking() {
		
	}
	public void liveChat() {
		
	}
	public void languages() {
		
	}
	public void notification() {
		
	}
	public void profileContent() {
	}
	public void profileContent_ManageLinkedAccounts() {
	}
	public void profileContent_PromoCode() {
	}
	public void profileContent_ChangePassword() {
		
	}
	public void profileContent_LoginAttempts() {
		
	}
	public void profileContent_ChangeProfilePicture() {
		try {
			if(lnk_change_profile_picture.isDisplayed()) {
				lnk_change_profile_picture.click();
				Thread.sleep(5000);
				/*
				Set<String>set = driver.getWindowHandles(); 
				Iterator<String>iterator = set.iterator(); 
				System.out.println(set.size());
				String win1 = iterator.next(); 
				driver.switchTo().window(win1);
				*/
				if(btn_choose_file_IN_profile_picture_upload.isDisplayed() && btn_choose_file_IN_profile_picture_upload.isEnabled()) {
				
						//btn_choose_file_IN_profile_picture_upload.click();
						//((JavascriptExecutor)driver).executeScript("document.getElementsByName('ProfilePicture').click()");
						((JavascriptExecutor)driver).executeScript("arguments[0].click();",btn_choose_file_IN_profile_picture_upload);
						//btn_choose_file_IN_profile_picture_upload.click();
						//btn_choose_file_IN_profile_picture_uploadCopy.click();
						//btn_choose_file_IN_profile_picture_upload.sendKeys("C://Users//User//Desktop//ProfPic.png");
						//((JavascriptExecutor)driver).executeScript("arguments[0].click();",btn_cancel_IN_profile_picture_upload);
						//btn_cancel_IN_profile_picture_upload.click();
						Thread.sleep(3000);
						Runtime.getRuntime().exec("C:\\Automation\\Selenium\\workspace\\atdd-expeedio-regression\\atdd-expeedio-regression\\src\\test\\resources\\com\\cnglobal\\expeedio\\utils\\Resources\\Prof_Pic_Upload_Script_FF");
						Thread.sleep(10000);
						((JavascriptExecutor)driver).executeScript("arguments[0].click();",btn_save_IN_profile_picture_upload);
						//btn_save_IN_profile_picture_upload.click();
						//Thread.sleep(5000);
						driver.switchTo().defaultContent();
				}
				
			}
			
			/*
			 * Set<String>set = driver.getWindowHandles(); Iterator<String>iterator =
			 * set.iterator(); String win1 = iterator.next(); String win2 = iterator.next();
			 * 
			 * if(set.size()>1) { driver.switchTo().window(win2);
			 * btn_choose_file_IN_profile_picture_upload.click(); }
			 */
				
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	public void profileContent_MySettings() {
		
	}

}
