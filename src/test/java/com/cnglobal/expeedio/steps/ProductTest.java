package com.cnglobal.expeedio.steps;

import java.io.IOException;
import java.lang.System.Logger;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.Assert;
import org.openqa.selenium.Cookie;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

import com.cnglobal.expeedio.pageobjects.FreightForward;
import com.cnglobal.expeedio.pageobjects.HomePage;
import com.cnglobal.expeedio.pageobjects.Products;
import com.cnglobal.expeedio.pageobjects.RouteOptimise;
import com.cnglobal.expeedio.utilities.Log;


public class ProductTest extends TestBase{
	
	String name,email,subject,message;
	//RouteOptimise routeOptimise = PageFactory.initElements(driver, RouteOptimise.class);
	//FreightForward freightForward = PageFactory.initElements(driver, FreightForward.class);
	RouteOptimise routeOptimise = new RouteOptimise(driver);
	FreightForward freightForward = new FreightForward(driver);
	Products products = new Products(driver,routeOptimise,freightForward);
	
	@Given("That i'm in expedio home page")
	public void that_i_m_in_expedio_home_page() throws IOException {
		init();
	}

	@When("I click route optimization link")
	public void i_click_route_optimization_link() {
		HomePage homepage = PageFactory.initElements(driver, HomePage.class);
		Log.info("Go to Route optimise page");
		homepage.goToRouteOptimise();
		
	}

	@Then("I should see route optimiztion page")
	public void i_should_see_route_optimiztion_page() {
		//RouteOptimise routeOptimise = PageFactory.initElements(driver, RouteOptimise.class);
		RouteOptimise routeOptimise = new RouteOptimise(driver);
		Assert.assertTrue(routeOptimise.isRouteOptimisePageDisplayed());
		//Products products = PageFactory.initElements(driver, Products.class);
		//Products products = new Products(driver,routeOptimise,freightForward);
		//Assert.assertTrue(products.getRouteOptimise().isRouteOptimisePageDisplayed());
		Log.info("I'm in Route optimise page");
	}
	
	@When("I enter name,email,subject,message and hit enter")
	public void i_enter_name_email_subject_message_and_hit_enter(List<String>formData) {
		RouteOptimise routeOptimise = PageFactory.initElements(driver, RouteOptimise.class);
		name = formData.get(0);
		email = formData.get(1);
		subject = formData.get(2);
		message = formData.get(3);
		routeOptimise.submitContactUsForm(name,email,subject,message);
		Log.info("Contact us form submited");
	}

	@Then("confirmation message should be given")
	public void confirmation_message_should_be_given() {
		RouteOptimise routeOptimise = PageFactory.initElements(driver, RouteOptimise.class);
		Assert.assertTrue(routeOptimise.isConfirmationMessageDisplayed());
		
	}

	@When("I click freight forward link")
	public void i_click_freight_forward_link() {
		HomePage homepage = PageFactory.initElements(driver, HomePage.class);
		Log.info("Go to Freight forward page");
		homepage.goToFreightForward();
		
	}

	@Then("I should see freight forward page")
	public void i_should_see_freight_forward_page() {
		FreightForward freightforward = PageFactory.initElements(driver, FreightForward.class);
		Assert.assertTrue(freightforward.isFreightForwardPageDisplayed());
		
		Log.info("I'm in freight forward page");
	}


}
