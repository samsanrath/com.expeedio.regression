package com.cnglobal.expeedio.steps;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.By;
import org.openqa.selenium.ElementNotVisibleException;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.cnglobal.expeedio.pageobjects.BookingPage;
import com.cnglobal.expeedio.pageobjects.HomePage;
import com.cnglobal.expeedio.utilities.Log;

import junit.framework.Assert;
import org.openqa.selenium.support.PageFactory;



public class GenerateQuotesTest extends TestBase {
	//WebDriver driver;

	String qty,type,weight,length,width,height;

	@Given("that I'm a expeedio user who is in expeedio home page")
	public void that_I_m_a_expeedio_user_who_is_in_expeedio_home_page() throws IOException {
		init();
	}

	@When("^I click on get instant quotes button$")
	public void i_click_on_get_instant_quotes_button() throws Throwable {
		HomePage homepage = PageFactory.initElements(driver, HomePage.class);
	}

	@Then("^I should go to generate quotes page$")
	public void i_should_go_to_generate_quotes_page() throws Throwable {
		//--------------   TBD   -------------------------------
	}


	@When("I click request quote button")
	public void i_click_request_quote_button() throws InterruptedException {
		HomePage homepage = PageFactory.initElements(driver, HomePage.class);
		homepage.goToBookingPageByUnknownUser();
		/*
		 * Actions actions= new Actions(driver);
		 * actions.keyDown(Keys.CONTROL).sendKeys(Keys.TAB).build().perform();
		 */
	}

	@SuppressWarnings("deprecation")
	@Then("I should go to the booking page")
	public void i_should_go_to_the_booking_page() {
		BookingPage bookingpage = PageFactory.initElements(driver, BookingPage.class);
		org.junit.Assert.assertTrue(bookingpage.isPageDisplayed());
	}

	@When("i enter pickup suburb")
	public void i_enter_pickup_suburb(List<String> pickupSubburbs) throws InterruptedException {
		try {
			String pickupSub = pickupSubburbs.get(0);
			BookingPage bookingpage = PageFactory.initElements(driver, BookingPage.class);
			/*
			 * Actions actions= new Actions(driver);
			 * actions.keyDown(Keys.CONTROL).sendKeys(Keys.TAB).build().perform();
			 */
			//ArrayList<String> tabs = new ArrayList<String>(driver.getWindowHandles());
			//driver.switchTo().window(tabs.get(1));
			Set<String> set = driver.getWindowHandles();
			Iterator<String> iterator =set.iterator();
			
			String mainWindow = iterator.next();
			String tabWindow = iterator.next();
			driver.switchTo().window(tabWindow);
			//Thread.sleep(5000);
			//WebDriverWait wait = new WebDriverWait(driver, 40);
			//wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//div[@class='form-group fromSuburb']/span/span/span/span/span")));
			bookingpage.selectPickupSub(pickupSub);
		} catch (Exception e) {
			Log.error(e.getMessage());
		}
	}

	@When("click pickup residential")
	public void click_pickup_residential() throws InterruptedException {
		BookingPage bookingpage = PageFactory.initElements(driver, BookingPage.class);
		bookingpage.clickPickupResidential();
	}


	@Then("pickup residential should be selected")
	public void pickup_residential_should_be_selected() {
		BookingPage bookingpage = PageFactory.initElements(driver, BookingPage.class);
		Assert.assertTrue(bookingpage.isToggleSelected(bookingpage.btn_residentialOfPickup));
	}
	//--------------------------------------------------

	@When("click pickup commercial")
	public void click_pickup_commercial() throws InterruptedException {
		BookingPage bookingpage = PageFactory.initElements(driver, BookingPage.class);
		bookingpage.click(bookingpage.btn_commercialOfPickup);
	}


	@Then("pickup commercial should be selected")
	public void pickup_commercial_should_be_selected() {
		BookingPage bookingpage = PageFactory.initElements(driver, BookingPage.class);
		Assert.assertTrue(bookingpage.isToggleSelected(bookingpage.btn_commercialOfPickup));
	}


	//--------------------------------------------
	@When("i enter delivery suburb")
	public void click_delivery_suburb(List<String> deliverySuburb) throws InterruptedException {
		String deliverySub = deliverySuburb.get(0);
		BookingPage bookingpage = PageFactory.initElements(driver, BookingPage.class);
		bookingpage.selectDeliverySub(deliverySub);
	}

	@When("click delivery residential")
	public void click_delivery_residential() throws InterruptedException {
		BookingPage bookingpage = PageFactory.initElements(driver, BookingPage.class);
		bookingpage.clickDeliveryResidential();
	}

	@Then("delivery residential should be selected")
	public void delivery_residential_should_be_selected() {
		BookingPage bookingpage = PageFactory.initElements(driver, BookingPage.class);
		Assert.assertTrue(bookingpage.isToggleSelected(bookingpage.btn_residentialOfPickup));
	}
	//------------------
	@When("click delivery commercial")
	public void click_delivery_commercial() throws InterruptedException {
		BookingPage bookingpage = PageFactory.initElements(driver, BookingPage.class);
		bookingpage.click(bookingpage.btn_commercialOfDelivery);
	}

	@Then("delivery commercial should be selected")
	public void delivery_commercial_should_be_selected() {
		BookingPage bookingpage = PageFactory.initElements(driver, BookingPage.class);
		Assert.assertTrue(bookingpage.isToggleSelected(bookingpage.btn_commercialOfDelivery));
	}
	//-----------------
	@When("i enter quatity type weight lenght width and height")
	public void i_enter_quatity_type_weight_lenght_width_and_height(List<String>packageInfo) {
		qty 	= packageInfo.get(0);
		type 	= packageInfo.get(1);
		length 	= packageInfo.get(2);
		width 	= packageInfo.get(3);
		height 	= packageInfo.get(4);
		weight 	= packageInfo.get(5);
		BookingPage bookingpage = PageFactory.initElements(driver, BookingPage.class);
		bookingpage.enterPackageDetails(qty,type,weight,length,width,height);
	}

	@Given("i need to add {int} more packages")
	public void i_need_to_add_more_packages(int numberOfPackages) throws InterruptedException {
		BookingPage bookingpage = PageFactory.initElements(driver, BookingPage.class);
		bookingpage.enterMorePackageDetails(numberOfPackages,qty,type,weight,length,width,height);
	}

	@Then("new row should be added")
	public void new_row_should_be_added() {
		BookingPage bookingpage = PageFactory.initElements(driver, BookingPage.class);
		Assert.assertTrue(bookingpage.isNewPackageRowAdded());
	}

	@When("i select dangerous goods checkbox")
	public void i_select_dangerous_goods_checkbox() {
		BookingPage bookingpage = PageFactory.initElements(driver, BookingPage.class);
		bookingpage.selectDangerousGoods();
	}

	@Then("dangerous goods checkbox should be selected")
	public void dangerous_goods_checkbox_should_be_selected() {
		BookingPage bookingpage = PageFactory.initElements(driver, BookingPage.class);
		Assert.assertTrue(bookingpage.DoisCheckBoxSelected(bookingpage.chkbx_dangerousGoods));
	}

	@When("i select {string} from dangerous goods dropdown")
	public void i_select_from_dangerous_goods_dropdown(String goodsType) {
		BookingPage bookingpage = PageFactory.initElements(driver, BookingPage.class);
		bookingpage.selectDangerousGoodsDropDown(goodsType);
	}

	@Then("the {string} should be selected in dangerous goods dropdown")
	public void the_should_be_selected_in_dangerous_goods_dropdown(String string) {
		BookingPage bookingpage = PageFactory.initElements(driver, BookingPage.class);
		Assert.assertTrue(bookingpage.isDrpdwnDesiredValueSelected(bookingpage.drpdwn_dangerousGoods,string));
	}


	@When("i select add tail lift checkbox")
	public void i_select_add_tail_lift_checkbox() throws InterruptedException {
		BookingPage bookingpage = PageFactory.initElements(driver, BookingPage.class);
		bookingpage.selectAddTailLift();
	}

	@Then("add tail lift checkbox should be selected")
	public void add_tail_lift_checkbox_should_be_selected() {
		BookingPage bookingpage = PageFactory.initElements(driver, BookingPage.class);
		Assert.assertTrue(bookingpage.DoisCheckBoxSelected(bookingpage.chkbx_addTailLift));
	}

	@When("i select {string} from tail lift drowpdown")
	public void i_select_from_tail_lift_drowpdown(String tailLiftTo) {
		BookingPage bookingpage = PageFactory.initElements(driver, BookingPage.class);
		bookingpage.selectTailLiftDropDown(tailLiftTo);
	}

	@Then("the {string} should be selected in tail lift drowpdown")
	public void the_should_be_selected_in_tail_lift_drowpdown(String string) {
		BookingPage bookingpage = PageFactory.initElements(driver, BookingPage.class);
		Assert.assertTrue(bookingpage.isDrpdwnDesiredValueSelected(bookingpage.drpdwn_addTailLift,string));
	}

	@When("i select authority to leave checkbox")
	public void i_select_authority_to_leave_checkbox() {
		BookingPage bookingpage = PageFactory.initElements(driver, BookingPage.class);
		bookingpage.selectAuthorityToLeave();
	}

	@Then("authority to leave checkbox should be selected")
	public void authority_to_leave_checkbox_should_be_selected() {
		BookingPage bookingpage = PageFactory.initElements(driver, BookingPage.class);
		Assert.assertTrue(bookingpage.DoisCheckBoxSelected(bookingpage.chkbx_authorityToLeave));
	}

	@When("i enter promo code")
	public void i_enter_promo_code() {
		//   ---------------   TBD   --------------
	}

	@When("click quote me button")
	public void click_quote_me_button() {
		BookingPage bookingpage = PageFactory.initElements(driver, BookingPage.class);
		bookingpage.quoteMe();
	}

	@Then("quotes should be generated")
	public void quotes_should_be_generated() throws InterruptedException {
		BookingPage bookingpage = PageFactory.initElements(driver, BookingPage.class);
		// TBD Thread.sleep(40000);
		FluentWait<WebDriver>fwait = new FluentWait<WebDriver>(driver)
				.withTimeout(80, TimeUnit.SECONDS)
				.pollingEvery(1, TimeUnit.SECONDS)
				.ignoring(NoSuchElementException.class);
		fwait.until(ExpectedConditions.visibilityOf(bookingpage.btn_bookNowFirst));
		Assert.assertTrue(bookingpage.isElementDisplayed(bookingpage.tblHeader_Carrier));
	}

	@When("i sort by price hitting price button")
	public void i_sort_by_price_hitting_price_button() {
		BookingPage bookingpage = PageFactory.initElements(driver, BookingPage.class);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		bookingpage.sortByPrice();
	}

	@SuppressWarnings("deprecation")
	@Then("lowest price should be on the top")
	public void lowest_price_should_be_on_the_top() throws InterruptedException {
		//try {
		BookingPage bookingpage = PageFactory.initElements(driver, BookingPage.class);
		//Thread.sleep(50000);
		ArrayList<String>section = new ArrayList<String>(driver.getWindowHandles());
		//System.out.println("Window handles are "+section);
		//driver.switchTo().window(section.get(1));
		//System.out.println("Current window handle is: "+driver.getWindowHandle().toString());
		Assert.assertTrue(bookingpage.isLowestPriceFirst(driver));
	}
}
