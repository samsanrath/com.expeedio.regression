package com.cnglobal.expeedio.steps;

import java.util.List;

import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.support.PageFactory;

import com.cnglobal.expeedio.pageobjects.DashBoardPage;
import com.cnglobal.expeedio.pageobjects.LoginPage;


public class UpdateProfilePictureTest extends TestBase{
	
	@When("I log in entering username and password")
	public void i_log_in_entering_username_and_password(List<String>credentials) {
		LoginPage loginpage = PageFactory.initElements(driver, LoginPage.class);
		String un = credentials.get(0);
		String pw = credentials.get(1);
		loginpage.login(un, pw);
		
	    // Write code here that turns the phrase above into concrete actions
	    // For automatic transformation, change DataTable to one of
	    // E, List<E>, List<List<E>>, List<Map<K,V>>, Map<K,V> or
	    // Map<K, List<V>>. E,K,V must be a String, Integer, Float,
	    // Double, Byte, Short, Long, BigInteger or BigDecimal.
	    //
	    // For other transformations you can register a DataTableType.
		
	}

	@When("and click on change profile picture")
	public void and_click_on_change_profile_picture() {
		DashBoardPage dashBoardPage = PageFactory.initElements(driver, DashBoardPage.class);
		dashBoardPage.doHandleUnknownWindow();
		dashBoardPage.getTopBar().img_profile_picture.click();
	}

	@Then("profile picture change window should appear")
	public void profile_picture_change_window_should_appear() {
	}

	@When("I click on choose file button and upload profile picture")
	public void i_click_on_choose_file_button_and_upload_profile_picture() {
		LoginPage loginpage = PageFactory.initElements(driver, LoginPage.class);
		DashBoardPage dashBoardPage = PageFactory.initElements(driver, DashBoardPage.class);
		dashBoardPage.getTopBar().profileContent_ChangeProfilePicture();
	}

	@Then("Profile picture should be updated")
	public void profile_picture_should_be_updated() {
	}

}
