package com.cnglobal.expeedio.steps;
import java.util.List;

import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.support.PageFactory;
import com.cnglobal.expeedio.pageobjects.DashBoardPage;
import com.cnglobal.expeedio.pageobjects.HomePage;
import com.cnglobal.expeedio.pageobjects.LoginPage;
//import com.cnglobal.expeedio.testbase.TestBase;
import com.cnglobal.expeedio.utilities.Log;


import junit.framework.Assert;

public class LoginTest extends TestBase{
	
	@When("I click login button")
	public void i_click_login_button() throws InterruptedException {
		HomePage homepage = PageFactory.initElements(driver, HomePage.class);
		homepage.goToLoginPage();
		Log.info("Goes to login page");

	}

	@Then("I should go to login page")
	public void i_should_go_to_login_page() {
		LoginPage loginpage = PageFactory.initElements(driver, LoginPage.class);
	
	    
	}
	@When("I enter username, password and hit enter")
	public void i_enter_username_name_password_and_hit_enter(List<String>credentials) {
		LoginPage loginpage = PageFactory.initElements(driver, LoginPage.class);
		String un = credentials.get(0);
		String pw = credentials.get(1);
		loginpage.login(un, pw);
		//Log.debug("******************************");
		/*
		 * Log.debug("******************************");
		 * Log.debug("BEFORE calling login function");
		 * Log.error("????????????????????????"); loginpage.login(un,pw);
		 * loginpage.getTopBar().profileContent();
		 * Log.debug("******************************");
		 * Log.debug("AFTER calling login function");
		 * Log.error("????????????????????????"); Log.trace("Trace"); Log.warn("warn");
		 */
	}
	
	@SuppressWarnings("deprecation")
	@Then("I should be in dashboard page")
	public void i_should_be_in_dashboard_page() {
		DashBoardPage dashBoardPage = PageFactory.initElements(driver, DashBoardPage.class);
		try {
			dashBoardPage.doHandleUnknownWindow();
		} catch (Exception e) {
		}
		Assert.assertTrue(dashBoardPage.isDashBoardDisplayed());
	}
	
}
