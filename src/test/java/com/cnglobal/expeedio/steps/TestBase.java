package com.cnglobal.expeedio.steps;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.events.EventFiringWebDriver;

import io.github.bonigarcia.wdm.WebDriverManager;

public class TestBase {
	public static Properties config = null;
	public static Properties OR = null;
	public static WebDriver webdriver = null;
	public static EventFiringWebDriver driver = null;
	public static boolean isloggedIn = false;
	
	
	public void init() throws IOException {
		
		if(driver == null) {
			// Config.properties file initialization.
			config = new Properties();
			FileInputStream fileinputstream = new FileInputStream(System.getProperty("user.dir")
					+"//src//test//resources//com//cnglobal//expeedio//utils//config.properties");
			config.load(fileinputstream);
			
			/*
			 * System.out.println(System.getProperty("user.dir")); // OR.properties file
			 * initialization FileInputStream fileinputstream1 = new
			 * FileInputStream(System.getProperty("user.dir")
			 * +"//src//test//resources//com//cnglobal//expeedio//expeedio//utils//OR.properties"
			 * ); OR.load(fileinputstream1);
			 */
			
			if (config.getProperty("browserType").equals("firefox")) {
				//System.setProperty("webdriver.gecko.driver", "C:\\Automation\\Selenium\\workspace\\Drivers\\geckodriver.exe");
				WebDriverManager.firefoxdriver().version("70.0.0").setup();
				webdriver = new FirefoxDriver();
			}else if (config.getProperty("browserType").equals("chrome")) {
				//DND   System.setProperty("webdriver.chrome.driver", "C:\\Automation\\Selenium\\workspace\\Drivers\\chromedriver.exe");
				//DND   
				WebDriverManager.chromedriver().version("83.0.4103.39").setup();
				webdriver = new ChromeDriver();
				//80.0.3987.16
				// 			--------IMPORTANT---------
				//WebDriverManager.chromedriver().setup();
				
				/*
				WebDriverManager.chromedriver()
                				.version("2.40")
                				.arch32()
                				.proxy("myproxyhostname:80")
                				.proxyUser("myusername")
                				.proxyPass("myawesomePassword")
                				.setup();
                */
				
				//	 		--------IMPORTANT ---------
				//DND   DesiredCapabilities desiredcapabilities = new DesiredCapabilities();
				//DND   desiredcapabilities.setCapability(capabilityName, value);
				//		 		--------IMPORTANT---------
				//DND   ChromeOptions options = new ChromeOptions();
				//DND   options.addArguments("--headless");
				//DND   webdriver = new ChromeDriver(options);
				
				
			}
			driver = new EventFiringWebDriver(webdriver);
			driver.manage().timeouts().implicitlyWait(2000, TimeUnit.SECONDS);
			driver.manage().window().maximize();
			driver.get(config.getProperty("site"));
		}
	}

}
