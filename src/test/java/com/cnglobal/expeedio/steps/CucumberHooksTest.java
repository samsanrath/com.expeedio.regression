package com.cnglobal.expeedio.steps;

import java.io.IOException;

import io.cucumber.java.After;
import io.cucumber.java.Before;
import org.junit.AfterClass;
import org.openqa.selenium.Alert;
import org.openqa.selenium.support.PageFactory;

import com.cnglobal.expeedio.pageobjects.DashBoardPage;
import com.cnglobal.expeedio.pageobjects.HomePage;
import com.cnglobal.expeedio.pageobjects.LoginPage;
import com.cnglobal.expeedio.utilities.Log;


public class CucumberHooksTest extends TestBase {
	//   Global hooks
	@Before(order=0)
	public void setUP() {
		try {
			Log.info("Starting webDriver initialization");
			init();
			//SoftAssertions softassertions;
		} catch (IOException e) {
			Log.error(e.getMessage());
		}
	}

	@After(order=0)
	public void tearDown() {
		Log.info("Driver is about to quit");
		driver.quit();
		driver=null;
		Log.info("Driver is null and did quit");
	}

	
	
	
	
	
	
	
	
	
	//   Local hooks
	@Before("@RouteOptimization")
	public void beforeRouteOptimization() {
		System.out.println("--- Before RouteOptimization---");
	}

	@After("@RouteOptimization")
	public void afterRouteOptimization() {
		System.out.println("--- After RouteOptimization---");
		Alert alert = driver.switchTo().alert();
	}
	/*
	@Before("@FreightForward")
	public void beforeFreightForward() {
		System.out.println("--- Before FreightForward---");
	}

	@After("@FreightForward")
	public void afterFreightForward() {
		System.out.println("--- After FreightForward---");
	}
	 */



	//---------  Below ONLY for Testing ------------

/*
	@Before("@Scenario1")
	public void beforeScenario1() {
		System.out.println("--- Before Scenario1---");
	}

	@After("@Scenario1")
	public void afterScenario1() {
		System.out.println("--- After Scenario1---");
	}
*/
}
