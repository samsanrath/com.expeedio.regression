package com.cnglobal.expeedio.steps;

import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.support.PageFactory;

import com.cnglobal.expeedio.pageobjects.PageBase;
import com.cnglobal.expeedio.pageobjects.DashBoardPage;
import com.cnglobal.expeedio.pageobjects.HomePage;


public class FindOutBrokenLinksTest extends TestBase{
	@When("I go to the home page")
	public void i_go_to_the_home_page() {
		HomePage homePage = PageFactory.initElements(driver, HomePage.class);
		homePage.doFindBrokenLinks();
	}

	@Then("All the broken links should be found")
	public void all_the_broken_links_should_be_found() {
		PageBase pagebase = PageFactory.initElements(driver, PageBase.class);
		pagebase.FindBrokenLinks();
	}

	@When("I go to the dashboard page")
	public void i_go_to_the_dashboard_page() {
		DashBoardPage dashBoardPage = PageFactory.initElements(driver, DashBoardPage.class);
		dashBoardPage.doFindBrokenLinks();
	}

}
