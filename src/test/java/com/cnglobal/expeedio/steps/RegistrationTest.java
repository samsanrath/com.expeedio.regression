package com.cnglobal.expeedio.steps;

import java.util.List;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.Assert;
import org.openqa.selenium.support.PageFactory;

import com.cnglobal.expeedio.pageobjects.DashBoardPage;
import com.cnglobal.expeedio.pageobjects.HomePage;
import com.cnglobal.expeedio.pageobjects.RegisterPage;
import com.cnglobal.expeedio.utilities.Log;



public class RegistrationTest extends TestBase {
	@When("I click signup button")
	public void i_click_sighup_button() {
	    HomePage homepage = PageFactory.initElements(driver, HomePage.class);
	    homepage.gotoRegisterPage();
	    
	}

	@Then("I should go to register page")
	public void i_should_go_to_register_page() {
		RegisterPage registerpage = PageFactory.initElements(driver, RegisterPage.class);
		DashBoardPage dashBoardPage = PageFactory.initElements(driver, DashBoardPage.class);
		Assert.assertTrue(registerpage.isElementDisplayed(registerpage.vfy_registerPage_lbl_customerSignUP));
		//Assert.assertTrue(dashBoardPage.isElementDisplayed(dashBoardPage.vfy_dashBoardPage_lnk_addAccount));
	}

	@Given("that I'm a expeedio user who is in expeedio register page")
	public void that_I_m_a_expeedio_user_who_is_in_expeedio_register_page() {
		
	}
	
	@When("I enter company name,first name,last name, email address,phone number,password and hit enter")
	public void i_enter_company_name_first_name_last_name_email_address_phone_number_password_and_hit_enter(List<String>formData) {
		try {
			String companyName,firstName,lastName,email,phoneNumber,password;
			RegisterPage registerpage = PageFactory.initElements(driver, RegisterPage.class);
			companyName = formData.get(0);
			firstName = formData.get(1);
			lastName = formData.get(2);
			email = formData.get(3);
			phoneNumber = formData.get(4);
			password = formData.get(5);
			registerpage.doRegister(companyName,firstName,lastName,email,phoneNumber,password);
		} catch (Exception e) {
			Log.error(e.getStackTrace());
		}
	}


	@Then("I should be able to successfully registerd")
	public void i_should_be_able_to_successfully_registerd() {
		DashBoardPage dashBoardPage = PageFactory.initElements(driver, DashBoardPage.class);
		Assert.assertTrue(dashBoardPage.isElementDisplayed(dashBoardPage.vfy_dashBoardPage_lnk_addAccount));
		
	}
	
	@When("I click personal radio button")
	public void i_click_personal_radio_button() {
		try {
			RegisterPage registerpage = PageFactory.initElements(driver, RegisterPage.class);
			registerpage.click(registerpage.radio_personal);
		} catch (Exception e) {
		}
	}

	@Then("Personal radio button should be selected")
	public void personal_radio_button_should_be_selected() {
		RegisterPage registerpage = PageFactory.initElements(driver, RegisterPage.class);
		//Assert.assertTrue(registerpage.isRadioBtnSelected(registerpage.radio_personal));
		Assert.assertTrue(registerpage.doisRadioBtnSelected());
		
	}
	
}
