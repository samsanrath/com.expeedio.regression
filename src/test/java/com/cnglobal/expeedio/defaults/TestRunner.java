package com.cnglobal.expeedio.defaults;

import org.junit.runner.RunWith;
import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;

@RunWith(Cucumber.class)
@CucumberOptions(
		plugin = {"pretty"},
		dryRun = false,
		monochrome = true,
		strict = true,
		glue = {"com.cnglobal.expeedio.steps"},
		features = {
				//"src/test/resources"
				//"C:/Automation/Selenium/workspace/atdd-expeedio-regression/atdd-expeedio-regression/src/test/resources/com/cnglobal/expeedio/features/EX-00001-Generate_Quotes.feature"
				//"src/test/resources/com/cnglobal/expeedio/features/EX-00001-Generate_Quotes.feature"
				//"src/test/resources/com/cnglobal/expeedio/features/EX-00002-Registration.feature"
				"src/test/resources/com/cnglobal/expeedio/features/EX-00003-login.feature"
				//"src/test/resources/com/cnglobal/expeedio/features/EX-00004-Verify Products.feature"
				//"src/test/resources/com/cnglobal/expeedio/features/EX-00005-Update_Profile.feature"
		},
		tags = {
				// OR      --->    {"@SmokeTest,@Regression"} Scenarios which has either of tags will be executed.
				// AND     --->    {"@SmokeTest","@Regression"} Scenarios which has both tags will be executed.
				// IGNORE  --->	   {~@E2E}
				
				//"@RouteOptimization,@FreightForward"
				//"@FreightForward"
				//"~@RouteOptimization"
				//"@BusinessCustomer"
				//"@PersonalCustomer"
				//"@Scenario1,@Scenario2,@Scenario3"
				//"@Scenario1"
		}
		
		)
public class TestRunner {
}
