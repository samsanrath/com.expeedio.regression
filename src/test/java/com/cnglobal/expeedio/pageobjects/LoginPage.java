package com.cnglobal.expeedio.pageobjects;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.cnglobal.expeedio.utilities.Log;

public class LoginPage extends PageBase{
	@FindBy(xpath="//input[@placeholder='User name or email']")
	public WebElement txt_userName;
	@FindBy(xpath="//input[@placeholder='Password']")
	public WebElement txt_password;
		
	WebDriver driver;
	public LoginPage(WebDriver driver) {
		super(driver);
	}
	public DashBoardPage login(String un, String pw) {
		try {
			txt_userName.sendKeys(un);
			txt_password.sendKeys(pw);
			txt_password.sendKeys(Keys.ENTER);
			Log.info("Logging successful");
		} catch (Exception e) {
			Log.error(e.getMessage());
			takeScreenShot();
		}
		return PageFactory.initElements(driver, DashBoardPage.class);
	}
}
