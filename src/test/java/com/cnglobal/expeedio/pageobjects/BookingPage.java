package com.cnglobal.expeedio.pageobjects;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.cnglobal.expeedio.utilities.Log;

public class BookingPage extends PageBase {
	// span[@id='select2-fromSuburb-container']
	// span[@id='select2-fromSuburb-container']//span[@class='select2-selection__placeholder'][contains(text(),'Select
	// Suburb')]
	// span[@class='select2 select2-container select2-container--default
	// select2-container--below']//span[@class='select2-selection
	// select2-selection--single']
	String qty, type, weight, length, width, height;
	//span[@id='select2-fromSuburb-container']//span[@class='select2-selection__placeholder'][contains(text(),'Select Suburb')]
	@FindBy(xpath = "//span[@id='select2-fromSuburb-container']//span[@class='select2-selection__placeholder'][contains(text(),'Select Suburb')]")
	//@FindBy(xpath = "//span[@id='select2-fromSuburb-container']")
	public WebElement drpdwn_pickupSuburb;

	@FindBy(xpath = "//input[@class='select2-search__field']")
	public WebElement drpdwn_pickupSearchField;
	
	
	@FindBy(xpath = "//div[@class='form-group toSuburb']/span/span/span")
	public WebElement drpdwn_deliverySuburb;
	@FindBy(xpath = "//input[@class='select2-search__field']")
	public WebElement drpdwn_deliverySearchField;
	// @FindBy(xpath = "//div[@id='pickupAddressType']//label[1]")
	@FindBy(xpath = "//form[@name='quoteForm']/div/div[1]/div[4]/div/label/input[@value='Residential']")
	public WebElement btn_residentialOfPickup;
	@FindBy(xpath = "//form[@name='quoteForm']/div/div[1]/div[4]/div/label[2]/input[@value='Business']")
	public WebElement btn_commercialOfPickup;
	@FindBy(xpath = "//form[@name='quoteForm']/div/div[2]/div[4]/div/label/input[@value='Residential']")
	public WebElement btn_residentialOfDelivery;
	@FindBy(xpath = "//form[@name='quoteForm']/div/div[2]/div[4]/div/label[2]/input[@value='Business']")
	public WebElement btn_commercialOfDelivery;
	@FindBy(xpath = "//input[@name='countInput']")
	public WebElement txt_packageQty;
	@FindBy(xpath = "//select[@id='typeInput']")
	public WebElement drpdwn_packageTyp;
	@FindBy(xpath = "//input[@id='weightInput']")
	public WebElement txt_packageWeight;
	@FindBy(xpath = "//input[@id='lengthInput']")
	public WebElement txt_packageLength;
	@FindBy(xpath = "//input[@id='widthInput']")
	public WebElement txt_packageWidth;
	@FindBy(xpath = "//input[@id='heightInput']")
	public WebElement txt_packageHeight;
	@FindBy(xpath = "//div[@class='col-md-11 offset-md-1']/button[contains(text(),'ADD ITEM')]")
	public WebElement btn_addItem;
	@FindBy(xpath = "//input[@id='isDangerousGood']")
	public WebElement chkbx_dangerousGoods;
	// @FindBy(xpath =
	// "//div[@class='row']//div[@class='col-md-12']//select[@class='form-control
	// ng-pristine ng-untouched ng-empty ng-invalid ng-invalid-required']")
	// public WebElement drpdwn_dangerousGoods;
	@FindBy(xpath = "//label[contains(text(),'Dangerous goods')]/../select")
	public WebElement drpdwn_dangerousGoods;
	@FindBy(xpath = "//input[@id='isTailLift']")
	public WebElement chkbx_addTailLift;
	@FindBy(xpath = "//div[@class='row']//div[@class='col-md-12']//select[@ng-options='item as item for item in tailLifOptions track by item']")
	public WebElement drpdwn_addTailLift;
	@FindBy(xpath = "//input[@id='isPoBox']")
	public WebElement chkbx_authorityToLeave;
	@FindBy(xpath = "//button[@id='btnQuoteMe']")
	public WebElement btn_quoteMe;
	@FindBy(xpath = "//button[contains(text(),'Price')]")
	public WebElement btn_sortByPrice;
	@FindBy(xpath = "//button[contains(text(),'Delivery Speed')]")
	public WebElement btn_sortByDeliverySpeed;
	@FindBy(xpath = "//button[contains(text(),'Earliest Collection')]")
	public WebElement btn_sortByEarliestCollection;
	@FindBy(xpath = "//input[@placeholder='Email Address']")
	public WebElement txt_enterEmail;
	@FindBy(xpath = "//button[contains(text(),'Email Quote')]")
	public WebElement btn_emailQuote;
	@FindBy(xpath = "//form[@name='quoteForm']/div[2]/div/div[3]/div/div/div[2]/div/select")
	public WebElement drpdwn_2ndPkgType;
	@FindBy(xpath = "//table[@class='table table-filter']/thead/tr/th[contains(text(),'Carrier')]")
	public WebElement tblHeader_Carrier;
	@FindBy(xpath = "//table[@class='table table-filter']/tbody")
	public WebElement tbl_leads;
	@FindBy(xpath = "//table/tbody/tr[1]/td[7]")
	public WebElement btn_bookNowFirst;

	WebDriver driver;

	public BookingPage(WebDriver driver) {
		super(driver);
	}
	
	public void selectPickupSub(String pickupSub) {
		try {
			do { 
				if(!drpdwn_pickupSuburb.isSelected()) {
					drpdwn_pickupSuburb.click();
					drpdwn_pickupSearchField.sendKeys(pickupSub);
					Thread.sleep(3000);
						drpdwn_pickupSearchField.sendKeys(Keys.ENTER);
						Log.info("Pickup suburb selected");
				}
			}
			while(drpdwn_pickupSuburb.isSelected()==true);
		} catch (Exception e) {
			System.out.println(e.getStackTrace());
			Log.error(e.getStackTrace()); 
			takeScreenShot();
		}
	}
	public void clickPickupResidential() throws InterruptedException {
		try {
			if (btn_residentialOfPickup.isDisplayed() == true) {
				btn_residentialOfPickup.click();
			}
		} catch (Exception e) {
			takeScreenShot();
			Log.error(e.getStackTrace());
		}
	}

	public void selectDeliverySub(String deliverySub) throws InterruptedException {
		try {
			drpdwn_deliverySuburb.click();
			drpdwn_deliverySearchField.sendKeys(deliverySub);
			Thread.sleep(2000);
			drpdwn_deliverySearchField.sendKeys(Keys.ENTER);
		} catch (Exception e) {
			takeScreenShot();
			Log.error(e.getStackTrace());
		}
	}

	public void clickDeliveryResidential() throws InterruptedException {
		try {
			Thread.sleep(3000);
			btn_residentialOfDelivery.click();
		} catch (Exception e) {
			takeScreenShot();
			Log.error(e.getStackTrace());
		}
	}

	public void enterPackageDetails(String qty, String type, String weight, String length, String width,
			String height) {
		try {
			this.qty = qty;
			this.type = type;
			this.weight = weight;
			this.length = length;
			this.width = width;
			this.height = height;
			Log.info("About to enter package details");
			enterText(txt_packageQty, qty);
			enterText(txt_packageLength, length);
			enterText(txt_packageWidth, width);
			enterText(txt_packageHeight, height);
			enterText(txt_packageWeight, weight);
			Log.info("package details entered");
		} catch (Exception e) {
			takeScreenShot();
			Log.error(e.getStackTrace());
		}
	}

	public void addItem() {
		try {
			if (btn_addItem.isEnabled()) {
				click(btn_addItem);
				Log.info("Add Item button clicked");
			}
		} catch (Exception e) {
			takeScreenShot();
			Log.error(e.getStackTrace());
		}
	}

	public void enterMorePackageDetails(int numberOfPackages, String qty, String type, String weight, String length,
			String width, String height) throws InterruptedException {
		int temp = numberOfPackages + 2;
		try {
			for (int i = 0; i < numberOfPackages; i++) {
				addItem();
				Thread.sleep(4000);
				// -----------------------
				/*
				 * txt_packageWeight.sendKeys("11"); WebElement elementt =
				 * driver.findElement(By.xpath(
				 * "//body[1]/div[1]/div[1]/section[1]/div[1]/div[1]/div[2]/section[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/form[1]/div[2]/div[1]/div[2]/div[1]/div[1]/div[6]/div[1]/div[1]/input[1]"
				 * )); elementt.sendKeys("22"); WebDriverWait wait = new WebDriverWait(driver,
				 * 20); wait.until(ExpectedConditions.elementToBeClickable(By.xpath(
				 * "//body[1]/div[1]/div[1]/section[1]/div[1]/div[1]/div[2]/section[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/form[1]/div[2]/div[1]/div["
				 * + temp + "]/div[1]/div[1]/div[6]/div[1]/div[1]/input[1]")));
				 * wait.until(ExpectedConditions.invisibilityOf(elementt)); Tried couple of
				 * hours with no luck
				 */
				// ----------------------
				
				txt_packageWeight.click();
				txt_packageWeight.sendKeys(Keys.TAB, Keys.TAB, Keys.TAB, Keys.TAB, length, Keys.TAB, width, Keys.TAB,
						height, Keys.TAB, weight);
			}
		} catch (Exception e) {
			takeScreenShot();
			Log.error(e.getLocalizedMessage());
		}
	}

	public void selectDangerousGoods() {
		try {
			if (chkbx_dangerousGoods.isSelected() == false) {
				clickByXpath(chkbx_dangerousGoods);
			}
		} catch (Exception e) {
			takeScreenShot();
			Log.error(e.getStackTrace());
		}
	}

	public void selectAddTailLift() throws InterruptedException {
		try {
			Thread.sleep(3000);
			clickByXpath(chkbx_addTailLift);
		} catch (Exception e) {
			takeScreenShot();
			Log.error(e.getStackTrace());
		}
	}

	public void selectAuthorityToLeave() {
		try {
			if (chkbx_authorityToLeave.isSelected() == false) {
				clickByXpath(chkbx_authorityToLeave);
			}
		} catch (Exception e) {
			takeScreenShot();
			Log.error(e.getStackTrace());
		}
	}

	public void quoteMe() {
		try {
			if (btn_quoteMe.isEnabled() == true) {
				clickByXpath(btn_quoteMe);
				Log.info("Quote me button clicked");
			}
		} catch (Exception e) {
			takeScreenShot();
			Log.error(e.getStackTrace());
		}
	}

	public void selectDangerousGoodsDropDown(String goodsType) {
		try {
			selectDropDownByIndex(drpdwn_dangerousGoods, 1);
			// goodsType
		} catch (Exception e) {
			takeScreenShot();
			Log.error(e.getStackTrace());
		}
	}

	public void selectTailLiftDropDown(String tailLiftTo) {
		try {
			selectDropDownByIndex(drpdwn_addTailLift, 1);
			// tailLiftTo
		} catch (Exception e) {
			takeScreenShot();
			Log.error(e.getStackTrace());
		}
	}

	public void sortByPrice() {
		try {
			// clickByXpath(btn_sortByPrice);
			btn_sortByPrice.click();
			Log.info("SortByPrice button clicked");
		} catch (Exception e) {
			takeScreenShot();
			Log.error(e.getStackTrace());
		}
	}

	public void sortByDeliverySpeed() {
		try {
			clickByXpath(btn_sortByDeliverySpeed);
		} catch (Exception e) {
			takeScreenShot();
			Log.error(e.getStackTrace());
		}
	}

	public void sortByEarliestCollection() {
		try {
			clickByXpath(btn_sortByEarliestCollection);
		} catch (Exception e) {
			takeScreenShot();
			Log.error(e.getStackTrace());
		}
	}

	public void temptest() {
		try {
			// driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
			// driver.findElement(By.xpath("//a[@class='btn btn-primary btn-sm
			// btn-color']")).click();
			// enterText(txt_enterEmail, "hisasarathnaweera");
			List<WebElement> quotes = new ArrayList<WebElement>();
			quotes = driver.findElements(By.xpath("//div[@class='table-container']"));
			System.out.println("Number of rows: " + quotes.size());
			// List<WebElement> quotes = driver.findElements(By.xpath("//table[@class='table
			// table-filter']/tbody"));
			// List<WebElement>quotes =
			// driver.findElements(By.xpath("//div[@class='col-lg-12 col-md-12
			// col-sm-12']"));
		} catch (Exception e) {
			takeScreenShot();
			Log.error(e.getStackTrace());
		}
	}

	public boolean isLowestPriceFirstCopy(WebDriver driver) {
		boolean flag = false;
		try {
			this.driver = driver;
			double minPrice = 0.00;
			double min;
			double temp = 0.00;

			List<String> prices = new ArrayList<String>();
			WebElement table = driver.findElement(By.xpath("//div[@class='row']/table/tbody"));
			// --->//div[@class='table-container']/table/tbody
			List<WebElement> rows = table.findElements(By.tagName("tr"));
			System.out.println("Number of rows: " + rows.size());

			List<WebElement> columns = rows.get(0).findElements(By.tagName("td"));
			System.out.println("Number of cols: " + columns.size());

			for (int row = 0; (row < (rows.size()) - 1); row++) {
				prices.add(row, driver.findElement(By.xpath(
						"//table[@class='table table-filter']//tbody/tr[" + (row + 1) + "]/td[6]/span[2]/strong"))
						.getText());

				System.out.println("List[" + row + "] = " + prices.get(row));
				minPrice = Double.parseDouble(prices.get(0));
				System.out.println("INIT MinPrice is " + minPrice);
				// min = Double.parseDouble(prices.get(0));
				// min =
				// Double.parseDouble(Double.toString(Double.parseDouble(prices.get(0))).replaceAll(",",
				// ""));
				// temp =
				// Double.parseDouble(Double.toString(Double.parseDouble(prices.get(row))).replaceAll(",",
				// ""));
				min = Double.parseDouble(prices.get(0));
				// temp = Double.parseDouble(prices.get(row));

				DecimalFormat decimalformat = new DecimalFormat();
				decimalformat.setGroupingUsed(false);
				String tempp = decimalformat.format(prices.get(row));
				temp = Double.parseDouble(tempp);
				// temp = Double.parseDouble(prices.get(row));
				// temp = Double.valueOf(prices.get(row));
				if (min <= temp) {
					System.out.println("Still min is " + min);
					flag = true;
				} else {
					min = temp;
					System.out.println("Min value is (temp) " + temp);
					flag = false;
				}
			}

		} catch (Exception e) {
			Log.error(e.getMessage());
			takeScreenShot();
		}
		return flag;
	}

	public boolean isLowestPriceFirst(WebDriver driver) {
		boolean flag = false;
		try {
			this.driver = driver;
			double minPrice = 0.00;
			double min;
			double temp = 0.00;
			// Thread.sleep(40000);
			WebElement table = driver.findElement(By.xpath("//div[@class='row']/table/tbody"));
			List<WebElement> rows = table.findElements(By.tagName("tr"));
			System.out.println("Number of rows: " + rows.size());
			List<Double> prices = new ArrayList<Double>();
			for (int row = 0; row < rows.size(); row++) {
				List<WebElement> columns = rows.get(row).findElements(By.tagName("td"));
				for (int col = 0; col < columns.size(); col++) {
					if (col == 5) {
						prices.add(row,
								(Double.parseDouble(driver
										.findElement(By.xpath("//table[@class='table table-filter']//tbody/tr["
												+ (row + 1) + "]/td[6]/span[2]/strong"))
										.getText().trim().replaceAll(",", ""))));
						System.out.println("Prices[" + row + "]= " + prices.get(row));
						min = prices.get(0);
						if (min <= prices.get(row)) {
							flag = true;
						} else {
							flag = false;
						}
					}
				}
			}
			/*
			 * System.out.println("List["+row+"] = " + prices.get(row)); minPrice =
			 * Double.parseDouble(prices.get(0)); System.out.println("INIT MinPrice is " +
			 * minPrice);
			 * 
			 * //min = Double.parseDouble(prices.get(0)); //min =
			 * Double.parseDouble(Double.toString(Double.parseDouble(prices.get(0))).
			 * replaceAll(",", "")); //temp =
			 * Double.parseDouble(Double.toString(Double.parseDouble(prices.get(row))).
			 * replaceAll(",", ""));
			 * 
			 * min = Double.parseDouble(prices.get(0)); //temp =
			 * Double.parseDouble(prices.get(row));
			 * 
			 * DecimalFormat decimalformat = new DecimalFormat();
			 * decimalformat.setGroupingUsed(false); String tempp =
			 * decimalformat.format(prices.get(row)); temp = Double.parseDouble(tempp);
			 * //temp = Double.parseDouble(prices.get(row)); //temp =
			 * Double.valueOf(prices.get(row)); if(min <= temp) {
			 * System.out.println("Still min is "+min); flag = true; }else { min = temp;
			 * System.out.println("Min value is (temp) "+temp); flag = false; }
			 */
		} catch (Exception e) {
			Log.error(e.getMessage());
			takeScreenShot();
		}
		return flag;
	}

	public boolean isToggleSelected(WebElement element) {
		boolean flag = false;
		try {
			if (element.getAttribute("class").contains("active")) {
				flag = true;
			} else {
				flag = false;
			}
		} catch (Exception e) {
			takeScreenShot();
			Log.error(e.getStackTrace());
		}
		return flag;
	}

	public boolean isNewPackageRowAdded() {
		return isElementDisplayed(drpdwn_2ndPkgType);
	}

	public boolean DoisCheckBoxSelected(WebElement element) {
		return isCheckBoxSelected(element);
	}

	public boolean DoisDrpdwnDesiredValueSelected(WebElement element, String string) {
		return isDrpdwnDesiredValueSelected(element, string);
	}
	public boolean isPageDisplayed(){
		return verifyPageByElement(btn_quoteMe);
	}
}
