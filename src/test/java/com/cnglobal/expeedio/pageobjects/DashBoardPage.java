package com.cnglobal.expeedio.pageobjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class DashBoardPage extends PageBase{
	WebDriver driver;
	public DashBoardPage(WebDriver driver) {
		super(driver);
	}
	
	@FindBy(xpath = "//span[@class='title'][contains(text(),'Dashboard')]" )
	public WebElement img_dashBoard;
	@FindBy(xpath = "//a[@href='/app/bookinginvoices']" )
	public WebElement vfy_dashBoardPage_lnk_addAccount;
    
	public void doHandleUnknownWindow() {
		HandleUnknownWindow(getTopBar().img_verify_dashboadpage);
	}

	public boolean isDashBoardDisplayed() {
		return isElementDisplayed(img_dashBoard);
	}

	public void doFindBrokenLinks() {
		FindBrokenLinks();
	}
	

}
