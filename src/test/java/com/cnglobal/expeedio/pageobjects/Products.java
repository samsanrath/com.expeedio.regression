package com.cnglobal.expeedio.pageobjects;

import org.openqa.selenium.WebDriver;

public class Products extends PageBase{
	WebDriver driver;
	FreightForward freightForward;
	RouteOptimise routeOptimise;
	
	public Products(WebDriver driver,RouteOptimise routeOptimise, FreightForward freightForward){
		super(driver);
		this.routeOptimise=routeOptimise; 
		this.freightForward=freightForward; 
	}

	
	public FreightForward getFreightForward() {
		return freightForward;
		
	}
	
	public RouteOptimise getRouteOptimise() {
		return routeOptimise;
		
	}
	


}
