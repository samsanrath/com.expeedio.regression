package com.cnglobal.expeedio.pageobjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class RegisterPage extends PageBase{
	public RegisterPage(WebDriver driver) {
		super(driver);
	}
	
	@FindBy(xpath = "//input[@name='CompanyName']")
	public WebElement txt_company;
	@FindBy(xpath = "//input[@name='FirstName']" )
	public WebElement txt_firstName;
	@FindBy(xpath = "//input[@name='LastName']" )
	public WebElement txt_lastName;
	@FindBy(xpath = "//input[@name='AdminEmailAddress']" )
	public WebElement txt_email;
	@FindBy(xpath = "//input[@name='PhoneNumber']" )
	public WebElement txt_phoneNumber;
	@FindBy(xpath = "//input[@name='AdminPassword']" )
	public WebElement txt_password;
	@FindBy(xpath = "//input[@name='AdminPasswordRepeat']" )
	public WebElement txt_reenterpassword;
	@FindBy(xpath = "//button[@type='submit']" )
	public WebElement btn_createAccount;
	@FindBy(xpath = "//div/h3[@class='m-login__title']" )
	public WebElement vfy_registerPage_lbl_customerSignUP;
	@FindBy(xpath = "//label[contains(text(),'Personal')]" )
	public WebElement radio_personal;
	
	

	public void doRegister(String companyName, String firstName, String lastName, String email, String phoneNumber, String password) {
		
		try {
			enterText(txt_company, companyName);
			enterText(txt_firstName, firstName);
			enterText(txt_lastName, lastName);
			enterText(txt_email, email);
			enterText(txt_phoneNumber, phoneNumber);
			enterText(txt_password, password);
			enterText(txt_reenterpassword, password);
			click(btn_createAccount);
		} catch (Exception e) {
		}
	}
	
	public boolean doisRadioBtnSelected() {
		return isRadioBtnSelected(radio_personal);
	}
}
