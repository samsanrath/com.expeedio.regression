package com.cnglobal.expeedio.pageobjects;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.io.FileHandler;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.cnglobal.expeedio.common.TopBar;
import com.cnglobal.expeedio.utilities.Log;

/*clickByXpath
 * selectDropDownByVisibleText
 * enterText
 */
public class PageBase {
	WebDriver driver;
	TopBar topbar;

	public PageBase(WebDriver driver) {
		this.driver = driver;
		topbar = PageFactory.initElements(driver, TopBar.class);
		// WebDriverWait wait = new WebDriverWait(driver, 100);
	}

	@FindBy(xpath = "//div[@class='swal-icon swal-icon--error']")
	public WebElement popup_logError;

	@FindBy(xpath = "//div[@class='swal-title']")
	public WebElement popup_logErrorTxt;

	public TopBar getTopBar() {
		return topbar;
	}

	public void click(WebElement element) {
		try {
			if (element.isEnabled()) {
				Log.info("About to click");
				element.click();
				Log.info("Element clicked");
			}

		} catch (Exception e) {
			Log.error(e.getStackTrace());
			takeScreenShot();
		}
	}

	// TBD deleted
	public void clickByXpath(WebElement element) {
		element.click();
	}
	// TBD deleted
	public void clickByLinkText(WebElement element) {
		element.click();
	}

	/*
	 * public void selectDropDownByVisibleText(WebElement element, String value) {
	 * element.click(); System.out.println(value); System.out.println(element);
	 * element.sendKeys(value); }
	 */

	public void selectDropDownByIndex(WebElement element, int index) {
		try {
			Select select = new Select(element);
			select.selectByIndex(index);
			Log.info("Dangerous goods drop down selected");
		} catch (Exception e) {
			takeScreenShot();
			Log.error(e.getStackTrace());
		}
	}

	public void enterText(WebElement element, String value) {
		try {
			if(element.isEnabled()) {
				Log.info("About to enter text");
				element.sendKeys(value);
				Log.info("Text entered");
			}
		} catch (Exception e) {
			Log.error(e.getStackTrace());
			takeScreenShot();
		}
	}

	public void mouseHover(WebElement element) {
		Actions actions = new Actions(driver);
		try {
			Thread.sleep(3000);
			actions.moveToElement(element).build().perform();
		} catch (InterruptedException e) {
			Log.error(e.getStackTrace());
			e.printStackTrace();
		}
	}

	public void takeScreenShot() {
		Date date = new Date();
		String screenShotFile = date.toString().replace(":", "_").replace(" ", "_") + ".png";
		File desFile = new File(System.getProperty("user.dir")+"//target//screenshots//"+screenShotFile);
		File scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		//TakesScreenshot -->INTERFACE & getScreenshotAs --> METHOD & output type would be a file.
		// FileUtils.copyFile();
		try {
			//FileUtils.copyfile(scrFile, new File(filePath));
			//FileHandler.copy(scrFile, new File(System.getProperty("user.dir")+"//target//screenshots//"+screenShotFile));
			FileHandler.copy(scrFile, desFile);
		} catch (IOException e) {
			Log.error(e.getStackTrace());
		}
	}

	public void HandleUnknownWindow(WebElement waitForThisElement) {
		try {
			Thread.sleep(5000);
			// driver.manage().timeouts().pageLoadTimeout(5, TimeUnit.SECONDS);
			// WebDriverWait wait = new WebDriverWait(driver, 10);
			// wait.until(ExpectedConditions.elementToBeClickable(waitForThisElement));
			Log.info("Handling popup window soon after login.");
			Set<String> set = driver.getWindowHandles();
			if (set.size() > 1) {
				return;
			} else {
				Actions action = new Actions(driver);
				action.sendKeys(Keys.ESCAPE);
				action.build().perform();
			}
		} catch (Throwable e) {
			System.out.println("Exception/Error " + e.getMessage());
			System.out.println(e.getStackTrace());
			Log.error(e.getMessage());
		}
	}

	public boolean verifyPageByTitle(String expectedTitle) {
		boolean flag = false;
		try {
			if (driver.getTitle().equalsIgnoreCase(expectedTitle)) {
				flag = true;
			} else {
				flag = false;
			}
		} catch (Exception e) {
			Log.error(e.getStackTrace());
		}
		return flag;
	}

	public boolean verifyConfirmationMessage(WebElement element, String message) {
		boolean flag = false;
		try {
			Thread.sleep(10000);
			WebDriverWait webdriverwait = new WebDriverWait(driver, 20);
			webdriverwait.until(ExpectedConditions.visibilityOf(element));
			//if (element.isDisplayed()==true && element.isEnabled()==true) {
			if (element.getText().equalsIgnoreCase(message)) {
				System.out.println("Message is " + element.getText());
				flag = true;
			} else {
				flag = false;
			}
			//}
		} catch (Exception e) {
			Log.error(e.getStackTrace());
			takeScreenShot();
		}
		return flag;
	}

	public boolean isElementDisplayed(WebElement element) {
		boolean flag = false;
		try {
			if (element.isDisplayed()) {
				//Log.info("Is element: " + element.getText() + " displayed: " + element.isDisplayed());
				flag = true;
			} else {
				//Log.info("is element: " + element.getText() + " displayed: " + element.isDisplayed());
				flag = false;
			}
		} catch (Exception e) {
			takeScreenShot();
			Log.error(e.getStackTrace());
		}
		return flag;
	}

	public boolean isElementEnable() {
		try {

		} catch (Exception e) {
		}
		return false;
	}

	public boolean isRadioBtnSelected(WebElement element) {
		boolean flag = false;
		try {
			if(element.getAttribute("Selected").equalsIgnoreCase("true")) {
				flag = true;

			}else {
				flag = false;
			}

		} catch (Exception e) {
			takeScreenShot();
			Log.error(e.getStackTrace());
		}
		return flag;
	}

	public boolean isCheckBoxSelected(WebElement element) {
		boolean flag = false;
		try {
			if(element.isSelected()) {
				flag = true;
			}else {
				flag = false;
			}
		} catch (Exception e) {
			takeScreenShot();
			Log.error(e.getStackTrace());
		}
		return flag;
	}

	public boolean isDrpdwnDesiredValueSelected(WebElement element,String string) {
		boolean flag = false;
		try {
			Select select = new Select(element);
			if (select.getFirstSelectedOption().getText().equalsIgnoreCase(string)) {
				flag = true;
			}else {
				flag = false;
			}
		} catch (Exception e) {
			takeScreenShot();
			Log.error(e.getStackTrace());
		}
		return flag;
	}

	public void FindBrokenLinks() {
		String url;
		String siteUrl = "https://expeedio.com.au/";
		HttpURLConnection huc = null;
		int respCode = 200;
		try {

			List<WebElement>links = driver.findElements(By.tagName("a"));
			Iterator<WebElement>it = links.iterator();
			while(it.hasNext()) {
				url = it.next().getAttribute("href");
				System.out.println(url);
				if(url==null || url.isBlank() || url.isEmpty()) {
					System.out.println(url+" is blank OR empty");
					continue;
				}if(!url.contains("expeedio")) {
					System.out.println(url+" belongs to third party");
					continue;
				}
				huc = (HttpURLConnection)(new URL(url).openConnection());
				huc.setRequestMethod("HEAD");
				huc.connect();
				respCode = huc.getResponseCode();
				if(respCode >= 400){
					System.out.println(url+" is a broken link"+ " & Response code is: "+ respCode);
				}
				else{
					//System.out.println(url+" is a valid link");
					continue;
				}
			}
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}


	}

	public void temp() {
		File file = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);

	}
	public boolean verifyPageByElement(WebElement element){
		boolean flag = false;
		try {
			if(element.isDisplayed()){
				flag = true;
			}else{
				flag = false;
			}
		}catch (Exception e){
		}
		return flag;
	}
}
