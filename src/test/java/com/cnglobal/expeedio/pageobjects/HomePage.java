package com.cnglobal.expeedio.pageobjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class HomePage extends PageBase{
	@FindBy(xpath="//a[@class='vc_general vc_btn3 vc_btn3-size-lg vc_btn3-shape-round vc_btn3-style-modern vc_btn3-icon-left vc_btn3-color-green']")
	public WebElement lnk_getInstantQuotes;
	
	@FindBy(xpath="//li[@id='menu-item-3450']//a[contains(text(),'Signup')]")
	public WebElement lnk_signup;
	
	@FindBy(xpath = "//li[@id='menu-item-3451']//a[contains(text(),'Login')]")
	public WebElement lnk_login;
	
	//@FindBy(xpath = "//li[@id='menu-item-3195']//a[contains(text(),'Request Quote')]")
	@FindBy(xpath = "//ul[@id=\"primary-menu\"]/li/a[contains(text(),'Request Quote')]")
	public WebElement lnk_requestQuote;
	
	@FindBy(linkText = "Route Optimisation and Transportation Logistics")
	public WebElement lnk_product_then_routeOptimisation;
	
	@FindBy(linkText = "Freight Forwarding")
	public WebElement lnk_product_then_freightForwarding;
	
	@FindBy(xpath = "//*[@id=\"menu-item-4063\"]/a[contains(text(),'Products')]")
	public WebElement lnk_product;
	
	@FindBy(xpath = "//li[@id='menu-item-2985']")
	public WebElement lnk_home;
	
	@FindBy(xpath = "//li[@id='menu-item-4031']")
	public WebElement lnk_affiliate;
	
	WebDriver driver;
	public HomePage(WebDriver driver) {
		super(driver);
	}
	
	/*
	 * public BookingPage gotoBookingPage() { lnk_getInstantQuotes.click(); return
	 * PageFactory.initElements(driver, BookingPage.class); }
	 */
	
	public RegisterPage gotoRegisterPage() {
		lnk_signup.click();
		return PageFactory.initElements(driver, RegisterPage.class);
	}
	
	public LoginPage goToLoginPage() throws InterruptedException {
		Thread.sleep(3000);
		lnk_login.click();
		return PageFactory.initElements(driver, LoginPage.class);
	}
	
	public BookingPage goToBookingPageByUnknownUser() throws InterruptedException {
		clickByXpath(lnk_requestQuote);
		return PageFactory.initElements(driver, BookingPage.class);
	}
	
	public RouteOptimise goToRouteOptimise() {
		mouseHover(lnk_product);
		try {
			Thread.sleep(8000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		lnk_product_then_routeOptimisation.click();
		return PageFactory.initElements(driver, RouteOptimise.class);
	}
	
	public FreightForward goToFreightForward() {
		mouseHover(lnk_product);
		click(lnk_product_then_freightForwarding);
		return PageFactory.initElements(driver, FreightForward.class);
	}

	public void doFindBrokenLinks() {
		FindBrokenLinks();
	}

}
