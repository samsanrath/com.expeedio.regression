package com.cnglobal.expeedio.pageobjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class RouteOptimise extends PageBase{
	WebDriver driver;
	public String name;
	public String email;
	public String subject;
	public String message;
	
	public RouteOptimise(WebDriver driver){
		super(driver);
		//this.driver = driver;
	}
	
	@FindBy(xpath = "//input[@name='your-name']")
	public WebElement txt_name;
	
	@FindBy(xpath = "//input[@name='your-email']")
	public WebElement txt_email;
	
	@FindBy(xpath = "//input[@name='your-subject']")
	public WebElement txt_subject;
	
	@FindBy(xpath = "//textarea[@name='your-message']")
	public WebElement txt_yourMessage;
	
	@FindBy(xpath = "//input[@type='submit']")
	public WebElement btn_send;
	
	@FindBy(xpath = "//input[@type='submit']/../../div[@role='alert']")
	public WebElement msg_formConfirmation;
	
	


	public void submitContactUsForm(String name,String email,String subject,String message) {
		enterText(txt_name, name);
		enterText(txt_email, email);
		enterText(txt_subject, subject);
		enterText(txt_yourMessage, message);
		click(btn_send);
		//txt_yourMessage.sendKeys(Keys.ENTER);
	}
	
	public String getName() {
		return name;
	}
	public String getEmail() {
		return email;
	}
	public String getSubject() {
		return subject;
	}
	public String getMessage() {
		return message;
	}
	public boolean isRouteOptimisePageDisplayed() {
		String title = "Route Optimisation and Transportation Logistics | Expeedio";
		return verifyPageByTitle(title);
	}
	public boolean isConfirmationMessageDisplayed() {
		String expected = "Thank you for your message. It has been sent.";
		return verifyConfirmationMessage(msg_formConfirmation,expected);
	}

	/*
	//--------------------------------------
	PageBase pagebase = new PageBase(driver);
	public void submitContactUsForm(String name,String email,String subject,String message) {
		
		
		pagebase.enterText(txt_name, name);
		pagebase.enterText(txt_email, email);
		pagebase.enterText(txt_subject, subject);
		pagebase.enterText(txt_yourMessage, message);
		pagebase.click(btn_send);
		//txt_yourMessage.sendKeys(Keys.ENTER);
	}
	
	public boolean isRouteOptimisePageDisplayed() {
		String title = "Route Optimisation and Transportation Logistics | Expeedio";
		return pagebase.verifyPageByTitle(title);
	}
	public boolean isConfirmationMessageDisplayed() {
		String expected = "Thank you for your message. It has been sent.";
		return pagebase.verifyConfirmationMessage(msg_formConfirmation,expected);
	}
	//-------------------------------
*/
}
