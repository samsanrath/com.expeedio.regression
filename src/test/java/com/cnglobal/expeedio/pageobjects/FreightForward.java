package com.cnglobal.expeedio.pageobjects;

import org.openqa.selenium.WebDriver;

public class FreightForward extends PageBase{
	WebDriver driver;
	public String name;
	public String email;
	public String subject;
	public String message;
	
	public FreightForward(WebDriver driver){
		super(driver);
	}
	
	public void contactFreightForward() {
		
	}
	
	public String getName() {
		return name;
	}

	public String getEmail() {
		return email;
	}

	public String getSubject() {
		return subject;
	}

	public String getMessage() {
		return message;
	}

	public boolean isFreightForwardPageDisplayed() {
		String title = "Road Freight Services Australia | Freight Forwarding Services | Expeedio";
		return verifyPageByTitle(title);
	}
	
}
