#Author: your.email@your.domain.com
#Keywords Summary :
#Feature: List of scenarios.
#Scenario: Business rule through list of steps with arguments.
#Given: Some precondition step
#When: Some key actions
#Then: To observe outcomes or validation
#And,But: To enumerate more Given,When,Then steps
#Scenario Outline: List of steps for data-driven as an Examples and <placeholder>
#Examples: Container for s table
#Background: List of steps run before each of the scenarios
#""" (Doc Strings)
#| (Data Tables)
#@ (Tags/Labels):To group Scenarios
#<> (placeholder)
#""
## (Comments)
#Sample Feature Definition Template
@Login
Feature: User login

Background:
#Given that I'm a expeedio user who is in expeedio home page
When I click login button
Then I should go to login page

 @Scenario1
  Scenario: Verify that user can login as personal customer
    When I enter username, password and hit enter 
   	| fname1lname1@cnglobal.com 					|     1qaz2wsx 					|
    Then I should be in dashboard page



      
 #    Examples: 
 #     | userName			  							| password 						|
 #     | fn1ln1@cnglobal.com 					|     qazwsx 					|

      
      
  