Feature: Generate quotes

Background:
#Given that I'm a expeedio user who is in expeedio home page
When I click request quote button
Then I should go to the booking page

@Scenario1
Scenario: Verify that unregistered user can generate residential quotes
When i enter pickup suburb
|	North st marys	|
And click pickup residential
Then pickup residential should be selected
When i enter delivery suburb
|	parramatta west	|
And click delivery residential
Then delivery residential should be selected
When i enter quatity type weight lenght width and height
|	44	|	BAG	|	2	|	3	|	4	|	5	|
And i need to add 1 more packages
Then new row should be added
When i select dangerous goods checkbox
Then dangerous goods checkbox should be selected
When i select "EXPLOSIVES" from dangerous goods dropdown
Then the "EXPLOSIVES" should be selected in dangerous goods dropdown
When i select add tail lift checkbox
Then add tail lift checkbox should be selected
When i select "Pickup" from tail lift drowpdown
Then the "Pickup" should be selected in tail lift drowpdown
When i select authority to leave checkbox
Then authority to leave checkbox should be selected
When i enter promo code
And click quote me button
Then quotes should be generated
When i sort by price hitting price button
Then lowest price should be on the top

@Scenario2
Scenario: Verify that unregistered user can generate commercial quotes
When i enter pickup suburb
|	North st marys	|
And click pickup commercial
Then pickup commercial should be selected
When i enter delivery suburb
|	parramatta west	|
And click delivery commercial
Then delivery commercial should be selected
When i enter quatity type weight lenght width and height
|	44	|	BAG	|	2	|	3	|	4	|	5	|
And i need to add 1 more packages
Then new row should be added
When i select dangerous goods checkbox
Then dangerous goods checkbox should be selected
When i select "EXPLOSIVES" from dangerous goods dropdown
Then the "EXPLOSIVES" should be selected in dangerous goods dropdown
When i select add tail lift checkbox
Then add tail lift checkbox should be selected
When i select "Pickup" from tail lift drowpdown
Then the "Pickup" should be selected in tail lift drowpdown
When i select authority to leave checkbox
Then authority to leave checkbox should be selected
When i enter promo code
And click quote me button
Then quotes should be generated
When i sort by price hitting price button
Then lowest price should be on the top

@Scenario3
Scenario: Verify that unregistered user can generate residential to commercial quotes
When i enter pickup suburb
|	North st marys	|
And click pickup residential
Then pickup residential should be selected
When i enter delivery suburb
|	parramatta west	|
And click delivery commercial
Then delivery commercial should be selected
When i enter quatity type weight lenght width and height
|	44	|	BAG	|	2	|	3	|	4	|	5	|
And i need to add 1 more packages
Then new row should be added
When i select dangerous goods checkbox
Then dangerous goods checkbox should be selected
When i select "EXPLOSIVES" from dangerous goods dropdown
Then the "EXPLOSIVES" should be selected in dangerous goods dropdown
When i select add tail lift checkbox
Then add tail lift checkbox should be selected
When i select "Pickup" from tail lift drowpdown
Then the "Pickup" should be selected in tail lift drowpdown
When i select authority to leave checkbox
Then authority to leave checkbox should be selected
When i enter promo code
And  click quote me button
Then quotes should be generated
When i sort by price hitting price button
Then lowest price should be on the top

#Scenario: Verify that unregistered user can generate commercial to residential quotes


#Scenario: Verify that known user can generate residential quotes


#Scenario: Verify that known user can generate commercial quotes


#Scenario: Verify that known user can generate residential to commercial quotes


#Scenario: Verify that known user can generate commercial to residential quotes