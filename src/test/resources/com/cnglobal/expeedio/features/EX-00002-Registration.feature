#Author: your.email@your.domain.com
#Keywords Summary :
#Feature: List of scenarios.
#Scenario: Business rule through list of steps with arguments.
#Given: Some precondition step
#When: Some key actions
#Then: To observe outcomes or validation
#And,But: To enumerate more Given,When,Then steps
#Scenario Outline: List of steps for data-driven as an Examples and <placeholder>
#Examples: Container for s table
#Background: List of steps run before each of the scenarios
#""" (Doc Strings)
#| (Data Tables)
#@ (Tags/Labels):To group Scenarios
#<> (placeholder)
#""
## (Comments)
#Sample Feature Definition Template

Feature: User registration

Background:
#Given that I'm a expeedio user who is in expeedio home page
When I click signup button
Then I should go to register page
 
  @BusinessCustomer
  Scenario: Verify that user can sign up as business customer
    When I enter company name,first name,last name, email address,phone number,password and hit enter
    |CompName		|fn		|ln		|email@abc.com		|0436899200		|123123|
    Then I should be able to successfully registerd
 
  @PersonalCustomer
  Scenario: Verify that user can sign up as business customer
  	When I click personal radio button
  	#Then Personal radio button should be selected
    When I enter company name,first name,last name, email address,phone number,password and hit enter
    |CompName		|fn		|ln		|email@abc.com		|0436899200		|123123|
    Then I should be able to successfully registerd
 