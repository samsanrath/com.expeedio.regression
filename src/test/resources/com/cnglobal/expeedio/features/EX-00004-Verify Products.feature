#Author: your.email@your.domain.com
#Keywords Summary :
#Feature: List of scenarios.
#Scenario: Business rule through list of steps with arguments.
#Given: Some precondition step
#When: Some key actions
#Then: To observe outcomes or validation
#And,But: To enumerate more Given,When,Then steps
#Scenario Outline: List of steps for data-driven as an Examples and <placeholder>
#Examples: Container for s table
#Background: List of steps run before each of the scenarios
#""" (Doc Strings)
#| (Data Tables)
#@ (Tags/Labels):To group Scenarios
#<> (placeholder)
#""
## (Comments)
#Sample Feature Definition Template



Feature: Go to products
  I want to use this template for my feature file

#Background: 
#Given That i'm in expedio home page

  @RouteOptimization
  Scenario: Verify route optimization
  When I click route optimization link
  Then I should see route optimiztion page
  When I enter name,email,subject,message and hit enter
 		|name	|emails@yahoo.com	|subject	|message	|
 		
  Then confirmation message should be given

  
  @FreightForward
  Scenario: Verify freight forward
  When I click freight forward link
  Then I should see freight forward page


#  Scenario Outline: Verify route optimise
#  When I click freight forward link
#  Then I should see freight forward page


 #   Examples: 
 #     | name  | value | status  |
 #     | name1 |     5 | success |
 #     | name2 |     7 | Fail    |
