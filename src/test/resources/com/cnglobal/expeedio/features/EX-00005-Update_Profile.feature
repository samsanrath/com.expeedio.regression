#Author: your.email@your.domain.com
#Keywords Summary :
#Feature: List of scenarios.
#Scenario: Business rule through list of steps with arguments.
#Given: Some precondition step
#When: Some key actions
#Then: To observe outcomes or validation
#And,But: To enumerate more Given,When,Then steps
#Scenario Outline: List of steps for data-driven as an Examples and <placeholder>
#Examples: Container for s table
#Background: List of steps run before each of the scenarios
#""" (Doc Strings)
#| (Data Tables)
#@ (Tags/Labels):To group Scenarios
#<> (placeholder)
#""
## (Comments)
#Sample Feature Definition Template

Feature: Update profile
  I want to use this template for my feature file

Background:
#Given that I'm a expeedio user who is in expeedio home page
When I click login button
Then I should go to login page

  @Scenario1
  Scenario: Verify that user can update profile picture
    When I log in entering username and password 
   	| fname1lname1@cnglobal.com 					|     1qaz2wsx 					|
    And and click on change profile picture
    Then profile picture change window should appear
    When I click on choose file button and upload profile picture
    Then Profile picture should be updated


